import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/* Micah Goff/ Running Buddy SLP / Nick Macias / CSE 223
 * 
 * UserOptions class contains methods to open, edit and create txt files required to display workouts, stored workout logs or user data via the
 * homepage.
 * */
public class UserOptions {
	userAccount myUser; // Saved userAccount variable that was passed on creation

	public UserOptions(userAccount user) {
		myUser = user;
	}

	//getFiles gets the names of the files stored in the users "logs" directory to be used to let the user decide which file to open in the
	//openLogsWindow. Has no parameters and returns an array of Strings that are the names of the stored files.
	public String[] getFiles() {
		String[] logName = { "" };
		File fo = new File(myUser.source + "\\logs\\"); //pathway to users log directory, using custom myUser.source pathway partially
		File[] files = fo.listFiles();
		if (files == null)
			return logName;
		logName = new String[files.length];
		for (int i = 0; i < files.length; i++) { //runs through the files in this for loop and stores them all in the logName String array
			logName[i] = files[i].getName();
		}
		return logName;
	}

	//openFile is used to open a specified txt file inside the users "logs" directory. Reads the txt file and creates a String of the read txt and
	//then returns said String.
	public String openFile(String fileName) {
		String log = " ";
		File fo = new File(myUser.source + "\\logs\\" + fileName);
		try {
			myUser.sc = new Scanner(fo);
			while (myUser.sc.hasNextLine()) { //builds String using concatenation
				log = log.concat(myUser.sc.nextLine() + System.lineSeparator());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return log; // returns built string from the txt file
	}

	//createLog is used to store a users logged workout and information from said workout into a txt file titled by the user
	public void createLog(String entry, String title, String runInfo) {
		File fo = new File(myUser.source + "\\logs");
		if (fo.exists() == false)
			fo.mkdir();
		fo = new File(myUser.source + "\\logs\\" + title + ".txt");
		try {
			myUser.pr = new PrintWriter(fo);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myUser.pr.println(entry + System.lineSeparator() + runInfo); //prints passed entry String and the runInfo String into the file specified
		myUser.pr.flush();
	}

	//openWorkouts does as the name entitles. It retrieves a certain workout txt file depending on what week the user is on and the tier they're
	//on as well. Builds a String that contains a whole weeks amount of workouts and returns the String.
	public String openWorkouts() {
		String source = myUser.source;
		source = source.replace("\\" + myUser.username + "\\", "\\_workouts_\\");

		int tier = (int) (myUser.skill);
		if (tier > 4)
			tier = 4;

		File fo = new File(source + "Tier " + tier + "\\Week " + myUser.week + ".txt"); //Path is based on the users skill and tier integer values
		try {
			myUser.sc = new Scanner(fo);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String workouts = "";
		while (myUser.sc.hasNextLine()) { // create workout text using concatenation
			workouts = workouts.concat(myUser.sc.nextLine() + System.lineSeparator());
		}
		workouts = workouts.replace("@", "");
		return workouts; //week worth of workouts returned
	}
}
