import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.WindowConstants;

//Window that contains information about warmingup before workouts/runs
public class stretchWindow extends JFrame {

	private JPanel contentPane;
	private JTextArea txtStandingKnee;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					stretchWindow frame = new stretchWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public stretchWindow() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 945, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		txtStandingKnee = new JTextArea();
		txtStandingKnee.setRows(3);
		txtStandingKnee.setWrapStyleWord(true);
		txtStandingKnee.setEditable(false);
		txtStandingKnee.setText(
				"The following should be completed before any major workouts such as tempos, intervals, fartleks, etc. \r\nThis will prevent injury and allow your body to function its best when running. These stretches are just the bare minimum, feel free to incorporate your own stretches!\r\n\r\nComplete a 1 mile or 1.5 mile warm up run. \r\n\r\n1. Standing knee grab followed by heel grab\r\n2. Grave diggers(Walking toe touches)\r\n3. Skipping arm circles\r\n4. Side ways skipping arm circles\r\n5. Toe walks(Switch between pointing in and pointing out)\r\n6. Heel walks(Switch between pointing in and pointing out)\r\n7. Karoke side steps\r\n8. Kicking toe touches\r\n9. Leg swings against wall\r\n10. Heel Squats\r\n\r\nA cool down run and stretches are also required after any and all workouts/runs.");
		contentPane.add(txtStandingKnee, BorderLayout.CENTER);
		txtStandingKnee.setColumns(10);
	}

}
