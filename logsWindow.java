import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.WindowConstants;
import javax.swing.JTextArea;

/*	Micah Goff/ Running Buddy SLP / Nick Macias / CSE 223
 * 	logsWindow is a window that is created when the user clicks the log a workout button on the homepage. Allows the user to create a txt
 * file that contains information about a completed workout.
 */
public class logsWindow extends JFrame {

	private JPanel contentPane;
	public JTextField hours;
	public JTextField minutes;
	public JTextField seconds;
	public JTextField distance;
	public JTextField heartrate;
	public JTextField title;
	public static userAccount myUser;
	private JTextArea entry;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					logsWindow frame = new logsWindow(myUser);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public logsWindow(userAccount user) {
		myUser = user;
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 756, 552);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton storeLog = new JButton("Log entry");
		storeLog.addMouseListener(new MouseAdapter() { //storeLog handles the scoring of the users logged workout and then saving this information
														// to the userAccount data variables.
			@Override
			public void mouseClicked(MouseEvent arg0) {
				myUser.windowOpen = false;
				int time = (Integer.parseInt(hours.getText())) * 60 + Integer.parseInt(minutes.getText())
						+ ((Integer.parseInt(seconds.getText())) / 60);
				double fitScore = myUser.getFit(time, Integer.parseInt(distance.getText()), //users information is scored here
						Integer.parseInt(heartrate.getText()));
				//System.out.println(fitScore);
				myUser.skill += fitScore;
				String runInfo = new String("Miles: " + distance.getText() + System.lineSeparator() + "Time: " //information passed is turned into a string to be used in the createLog method
						+ hours.getText() + ":" + minutes.getText() + ":" + seconds.getText());
				UserOptions logs = new UserOptions(user);
				logs.createLog(entry.getText(), title.getText(), runInfo); //creates txt file containing this workouts info
				dispose();
			}
		});
		storeLog.setFont(new Font("Tahoma", Font.PLAIN, 20));
		storeLog.setBounds(447, 416, 150, 76);
		contentPane.add(storeLog);

		hours = new JTextField();
		hours.setBounds(87, 446, 55, 22);
		contentPane.add(hours);
		hours.setColumns(10);

		minutes = new JTextField();
		minutes.setColumns(10);
		minutes.setBounds(154, 446, 55, 22);
		contentPane.add(minutes);

		seconds = new JTextField();
		seconds.setColumns(10);
		seconds.setBounds(221, 446, 55, 22);
		contentPane.add(seconds);

		JLabel lblNewLabel = new JLabel("Hours");
		lblNewLabel.setBounds(87, 417, 56, 16);
		contentPane.add(lblNewLabel);

		JLabel lblMinutes = new JLabel("Minutes");
		lblMinutes.setBounds(154, 416, 56, 16);
		contentPane.add(lblMinutes);

		JLabel lblNewLabel_1_1 = new JLabel("Seconds");
		lblNewLabel_1_1.setBounds(221, 416, 56, 16);
		contentPane.add(lblNewLabel_1_1);

		JLabel lblDistance = new JLabel("Distance");
		lblDistance.setBounds(12, 416, 56, 16);
		contentPane.add(lblDistance);

		distance = new JTextField();
		distance.setColumns(10);
		distance.setBounds(12, 446, 55, 22);
		contentPane.add(distance);

		JLabel lblNewLabel_1_1_1 = new JLabel("Heartrate(optional)");
		lblNewLabel_1_1_1.setBounds(326, 416, 109, 16);
		contentPane.add(lblNewLabel_1_1_1);

		heartrate = new JTextField("0");
		heartrate.setColumns(10);
		heartrate.setBounds(326, 446, 78, 22);
		contentPane.add(heartrate);

		JLabel lblEnterInThe = new JLabel(
				"Enter in the text box down below how your workout went and then hit \"Log entry\" when your done!");
		lblEnterInThe.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		lblEnterInThe.setBounds(22, 13, 704, 53);
		contentPane.add(lblEnterInThe);

		title = new JTextField();
		title.setText("Enter title of entry here");
		title.setBounds(12, 64, 197, 22);
		contentPane.add(title);
		title.setColumns(10);

		entry = new JTextArea();
		entry.setLineWrap(true);
		entry.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		entry.setBounds(12, 99, 690, 304);
		contentPane.add(entry);

		JButton quitOut = new JButton("Quit");
		quitOut.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				myUser.windowOpen = false;
				dispose();
			}
		});
		quitOut.setFont(new Font("Tahoma", Font.PLAIN, 20));
		quitOut.setBounds(628, 459, 98, 33);
		contentPane.add(quitOut);
	}

}
