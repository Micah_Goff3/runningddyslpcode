import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.io.PrintWriter;
import java.util.Scanner;

/*		Micah Goff/ Running Buddy SLP / Nick Macias / CSE 223
 *  	userAccount class used to store all the data taken from the info file that is opened when the user logs in. Methods inside this class
 *  	are used to retrieve information about the user whether it be the week of workouts theyre on or score their run they just logged.
 * */
public class userAccount {
	String username;
	int password;
	double skill = 1;
	int week = 1;
	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");  //Data variables within the users account as well as PrintWriter and Scanners
	Date login = new Date();										  //used in the UserOptions class
	String source = "";
	PrintWriter pr;
	Scanner sc;
	String startOfWeek = " ";
	boolean windowOpen = false;

	//userAcct sets the username and password so that it can be later used by the login method. If the newUser boolean is set to true then a new
	//account is created using the username and password passed.
	public boolean userAcct(String username, String password, boolean newUser) {
		this.username = username;
		this.password = hashIt(password);
		source = "C:\\Users\\" + System.getProperty("user.name")
				+ "\\Desktop\\RunningBuddy\\userlogs\\" + username + "\\";
		if (newUser) {
			boolean flag = createAccount();
			return flag;
		}
		return true;
	}

	//hashIt creates an int value out of the String that is passed to it. Used on the users password to prevent password cracking
	public int hashIt(String key) {
		int hash = 0;
		for (int i = 0; i < key.length(); i++) {
			hash = (hash + key.charAt(i)) * key.charAt(i);
		}
		return hash;
	}

	//login searches for a txt file based on the username variable. If found then the file is checked to see if the username and password given
	//is correct or not.
	public boolean login() {
		try {
			File userInfo = new File(source + username + "_info.txt");
			sc = new Scanner(userInfo);
			for (int i = 0; i < 5; i++) {
				switch (i) {
				case 0:
					if (username.compareTo(sc.nextLine()) != 0)
						return false;
					break;
				case 1:
					if (password != Integer.parseInt(sc.nextLine()))
						return false;
					break;
				case 2:
					skill = Double.parseDouble(sc.nextLine());
					break;
				case 3:
					week = Integer.parseInt(sc.nextLine());
					break;
				case 4:
					startOfWeek = sc.nextLine();
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	//createAccount uses the data variables stored above to create a new directory in the userlogs file. this directory is then populated 
	//by a single txt file containing the users username, password in hashed form, their skill level, current week of tier theyre on and
	//the current date
	public boolean createAccount() {
		try {
			File drct = new File(source);
			if (drct.exists() == false)
				drct.mkdir();

			File userFolder = new File(source);
			File userInfo = new File(source + username + "_info.txt");
			if (userInfo.exists())
				return false;
			userFolder.mkdirs();
			pr = new PrintWriter(userInfo);
			pr.println(username + "\n" + password + "\n" + skill + "\n" + week + "\n" + formatter.format(login));
			pr.flush();

			System.out.println("Account created");
			return true;
		} catch (Exception e) {
			System.out.println("File error: " + e);
			return false;
		}
	}

	//getFit method is used to give a workout a certain amount of points that is then added onto the users skill score. The points recieved
	//is based off the time, distance, heart rate and pace of the workout the user logs. Returns a double value.
	public double getFit(double time, double dist, double hr) {
		double fitScore = 0;
		double pace = time / dist;
		System.out.println(pace);

		if (dist > 0 && dist < 5)
			fitScore += .015;
		if (dist > 4 && dist < 10)
			fitScore += .016;
		if (dist > 9 && dist < 15) //distance is based on a scale of 1 to 20+ miles cut into 4 sections that award a different amount of points
			fitScore += .0117;
		if (dist > 14 && dist < 20)
			fitScore += .018;
		if (dist > 20)
			fitScore += .015;

		if (time > 8 && time < 40)
			fitScore += .015;
		if (time > 40 && time < 80)
			fitScore += .016;
		if (time > 80 && time < 120)  //time is based on a scale of 8 to 160 minutes cut into 4 sections that award a different amount of points
			fitScore += .017;
		if (time > 120 && time < 160)
			fitScore += .016;
		if (time > 160)
			fitScore += .005;

		if (hr > 104 && hr < 114)
			fitScore += .015;
		if (hr > 114 && hr < 133)
			fitScore += .016;
		if (hr > 133 && hr < 152)  //heart rate is based on a scale of 1 to 20+ miles cut into 4 sections that award a different amount of points
			fitScore += .017;
		if (hr > 171 && hr < 190)
			fitScore += .018;
		if (hr > 190)
			fitScore += .015;

		if (pace > 8 && pace < 11)
			fitScore += .010;
		if (pace > 7.5 && pace < 9.5)  //pace is based on a scale of  to 8min/mile to 4:30min/mile cut into 4 sections that award a different amount of points
			fitScore += .011;
		if (pace > 7 && pace < 8.5)
			fitScore += .012;
		if (pace > 5.5 && pace < 6.5)
			fitScore += .011;
		if (pace < 4.5)
			fitScore += .010;

		return fitScore;
	}

	//saveInfo writes the users info to txt file when the application is being shut down
	public void saveInfo() {
		File fo = new File(source + username + "_info.txt"); //path to users_info.txt file
		try {
			pr = new PrintWriter(fo);
			pr.println(username + "\n" + password + "\n" + skill + "\n" + week + "\n" + startOfWeek); //format the info is saved in is important
			pr.flush();																				  //as its read a certain way during login
			pr.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//getWeek checks when the user started this week of workouts. if more than a week has passed since the start date then
	//the week value is changed in the userAccount and thus the workouts are changed. This new start date is saved to file later on.
	public void getWeek() {
		Date start = new Date();
		Date today = new Date();
		Date next = new Date();
		try {
			start = new SimpleDateFormat("MM/dd/yyyy").parse(startOfWeek);
			next = new SimpleDateFormat("MM/dd/yyyy").parse(startOfWeek);
			next.setTime(start.getTime() + 604800000); //gets the date a week from the start date of this workout week
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		if (today.after(next)) {
			if (week == 4)
				week = 0;
			week += 1;
			start = new Date();
			startOfWeek = formatter.format(start); //sets the new start date to the current day
			return;
		}
	}
}
