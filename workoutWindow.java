import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

//creates a window that allows the user to view their week of workouts. also gives the user access to a page containing warm up info and stretches
public class workoutWindow extends JFrame {

	private JPanel contentPane;
	public static userAccount myUser;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					workoutWindow frame = new workoutWindow(myUser);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public workoutWindow(userAccount user) {
		myUser = user;
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 756, 497);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTextPane textPane = new JTextPane();
		textPane.setFont(new Font("Times New Roman", Font.PLAIN, 19));
		UserOptions opt = new UserOptions(myUser);
		textPane.setText(opt.openWorkouts());
		textPane.setEditable(false);
		textPane.setBounds(12, 67, 714, 331);
		contentPane.add(textPane);

		JLabel lblNewLabel = new JLabel("View this weeks workouts and hit \"Quit\" when youre done!");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		lblNewLabel.setBounds(28, 13, 396, 41);
		contentPane.add(lblNewLabel);

		JButton quitOut = new JButton("Quit");
		quitOut.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				myUser.windowOpen = false;
				dispose();
			}
		});
		quitOut.setBounds(617, 411, 97, 25);
		contentPane.add(quitOut);

		JButton btnOpenStretchingList = new JButton("Warm Up Instructions");
		btnOpenStretchingList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { //opens the stretches window
				stretchWindow sw = new stretchWindow();
				sw.setVisible(true);
			}
		});
		btnOpenStretchingList.setBounds(557, 22, 157, 25);
		contentPane.add(btnOpenStretchingList);

	}
}
