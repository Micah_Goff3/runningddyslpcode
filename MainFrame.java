import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;
import java.awt.Font;


//the MainFrame class is the window that initially opens when the program is first launched. Allows the user to login or create account
//depending on their press of buttons
public class MainFrame extends JFrame {

	private JPanel contentPane;
	private JTextField username;
	private JTextField passwordField;
	boolean userFlag = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setBackground(Color.LIGHT_GRAY);
		setForeground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 586, 463);
		contentPane = new JPanel();
		contentPane.setForeground(Color.RED);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		username = new JTextField();
		username.setBounds(55, 208, 116, 22);
		contentPane.add(username);
		username.setColumns(10);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setVerticalAlignment(SwingConstants.TOP);
		lblUsername.setHorizontalAlignment(SwingConstants.LEFT);
		lblUsername.setBounds(55, 154, 171, 41);
		contentPane.add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setVerticalAlignment(SwingConstants.TOP);
		lblPassword.setHorizontalAlignment(SwingConstants.LEFT);
		lblPassword.setBounds(364, 154, 166, 41);
		contentPane.add(lblPassword);

		JLabel pageMessage = new JLabel("Running Buddy login");
		pageMessage.setVerticalAlignment(SwingConstants.TOP);
		pageMessage.setHorizontalAlignment(SwingConstants.LEFT);
		pageMessage.setFont(new Font("Tahoma", Font.PLAIN, 22));
		pageMessage.setBounds(167, 69, 277, 72);
		contentPane.add(pageMessage);

		JButton loginBtn = new JButton("Login");
		loginBtn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		loginBtn.setBounds(186, 254, 166, 25);
		contentPane.add(loginBtn);

		JButton quitBtn = new JButton("Quit");
		quitBtn.addMouseListener(new MouseAdapter() { //closes out the login page 
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
		quitBtn.setBounds(459, 378, 97, 25);
		contentPane.add(quitBtn);

		JButton createAccount = new JButton("Create Account");
		createAccount.setBounds(186, 317, 166, 48);
		contentPane.add(createAccount);

		passwordField = new JTextField();
		passwordField.setColumns(10);
		passwordField.setBounds(364, 208, 116, 22);
		contentPane.add(passwordField);

		JLabel errorMessage = new JLabel("Username taken");
		errorMessage.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		errorMessage.setBounds(200, 198, 152, 41);
		contentPane.add(errorMessage);
		errorMessage.setVisible(false);

		// event handlers
		createAccount.addMouseListener(new MouseAdapter() { // create account changes to the create account page
			@Override
			public void mouseClicked(MouseEvent e) {
				userFlag = true;
				createAccount.setVisible(false);
				loginBtn.setText("Create Account");
				lblUsername.setText("Enter desired username");
				lblPassword.setText("Enter desired password");
			}
		});

		loginBtn.addMouseListener(new MouseAdapter() { // handles creating or logging into the users account 
													   //and then calling to create the users homepage window
			@Override
			public void mouseClicked(MouseEvent arg0) {
				errorMessage.setVisible(false);
				userAccount user = new userAccount();
				userFlag = user.userAcct(username.getText(), passwordField.getText(), userFlag);
				if (userFlag == false) {
					errorMessage.setVisible(true);
				}
				boolean login = user.login();
				if (login) {
					homePage page = new homePage(user);
					page.setVisible(true);
					dispose();
				} else {
					errorMessage.setText("Login unsuccesful");
					errorMessage.setVisible(true);
				}
			}
		});
	}
}
