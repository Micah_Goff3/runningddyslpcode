import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

//creates a window where the user can select a txt file to open and view a stored workout
public class openLogsWindow extends JFrame {

	private JPanel contentPane;
	public static userAccount myUser;
	JTextArea txtPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					openLogsWindow frame = new openLogsWindow(myUser);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("unchecked")
	public openLogsWindow(userAccount user) {
		myUser = user;
		UserOptions uo = new UserOptions(myUser); 
		String[] fileNames = uo.getFiles();			//getFiles method is used to get the users stored file names and then added to ComboBox object
		DefaultComboBoxModel bm = new DefaultComboBoxModel();
		for (int i = 0; i < fileNames.length; i++) {
			bm.addElement(fileNames[i]);
		}

		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 630, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(bm);
		comboBox.setBounds(135, 13, 297, 22);
		contentPane.add(comboBox);

		JButton openLog = new JButton("Open Log");
		openLog.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtPane.setText(uo.openFile(fileNames[comboBox.getSelectedIndex()])); //sets txt to the returned string of openFile that is the txt found in the file specified by the fileNames array
				txtPane.setVisible(true);
			}
		});
		openLog.setBounds(472, 12, 97, 25);
		contentPane.add(openLog);

		JButton openLog_1 = new JButton("Quit");
		openLog_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				myUser.windowOpen = false;
				dispose();
			}
		});
		openLog_1.setBounds(503, 415, 97, 25);
		contentPane.add(openLog_1);

		txtPane = new JTextArea();
		txtPane.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		txtPane.setLineWrap(true);
		txtPane.setBounds(12, 48, 588, 358);
		contentPane.add(txtPane);
		txtPane.setVisible(false);

	}
}
