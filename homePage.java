import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class homePage extends JFrame {

	private JPanel contentPane;
	public static userAccount myUser;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					homePage frame = new homePage(myUser);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public homePage(userAccount user) {
		myUser = user;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 720, 498);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Your Skill: " + myUser.skill + "  /  Week #: " + myUser.week + "  /  Tier Level: "+ (int)myUser.skill + "  /  Started workout week on: " + myUser.startOfWeek);
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 19));
		lblNewLabel.setBounds(5, 5, 692, 41);
		contentPane.add(lblNewLabel);
		
		JButton workouts = new JButton("View this weeks workouts!");
		workouts.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		workouts.setBounds(5, 84, 206, 78);
		contentPane.add(workouts);
		
		JButton logs = new JButton("View logged workouts!");
		logs.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		logs.setBounds(5, 194, 206, 78);
		contentPane.add(logs);
		
		JButton makeLogs = new JButton("Log a workout!");
		makeLogs.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		makeLogs.setBounds(5, 307, 206, 78);
		contentPane.add(makeLogs);
		
		JButton quitOut = new JButton("Quit");
		quitOut.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		quitOut.setBounds(573, 384, 117, 54);
		contentPane.add(quitOut);
		
		logs.addMouseListener(new MouseAdapter() { //create account
			@Override
			public void mouseClicked(MouseEvent e) {
				if(user.windowOpen)return;
				user.windowOpen = true;
				openLogsWindow openLogs = new openLogsWindow(user);
				openLogs.setVisible(true);
			}
		});
		quitOut.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				user.saveInfo();
				System.exit(0);
			}
		});
		
		makeLogs.addMouseListener(new MouseAdapter() { //create account
			@Override
			public void mouseClicked(MouseEvent e) {
				if(user.windowOpen)return;
				user.windowOpen = true;
				logsWindow logWindow = new logsWindow(user);
				logWindow.setVisible(true);
			}
		});
		
		workouts.addMouseListener(new MouseAdapter() { //create account
			@Override
			public void mouseClicked(MouseEvent e) {
				if(user.windowOpen)return;
				user.windowOpen = true;
				myUser.getWeek();
				workoutWindow ww = new workoutWindow(user);
				ww.setVisible(true);
			}
		});
	}
}

